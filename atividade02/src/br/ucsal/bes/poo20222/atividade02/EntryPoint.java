package br.ucsal.bes.poo20222.atividade02;

import br.ucsal.bes.poo20222.atividade02.tui.MenuTUI;

public class EntryPoint {

	public static void main(String[] args) throws Exception {
		MenuTUI.executar();
	}

}
