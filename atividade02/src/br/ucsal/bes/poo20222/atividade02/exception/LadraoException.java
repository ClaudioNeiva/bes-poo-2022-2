package br.ucsal.bes.poo20222.atividade02.exception;

public class LadraoException extends Exception {

	private static final long serialVersionUID = 1L;

	public LadraoException(String message) {
		super(message);
	}
	
}
