package br.ucsal.bes.poo20222.atividade02.exception;

public class ObjetoNaoEncontradoException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public ObjetoNaoEncontradoException(String message) {
		super(message);
	}
	
}
