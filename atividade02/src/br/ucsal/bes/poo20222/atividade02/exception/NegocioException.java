package br.ucsal.bes.poo20222.atividade02.exception;

public class NegocioException extends Exception {

	private static final long serialVersionUID = 1L;

	public NegocioException(String message) {
		super(message);
	}

}
