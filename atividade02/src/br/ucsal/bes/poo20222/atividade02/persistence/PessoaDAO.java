package br.ucsal.bes.poo20222.atividade02.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes.poo20222.atividade02.domain.Pessoa;
import br.ucsal.bes.poo20222.atividade02.exception.NegocioException;

public class PessoaDAO {

	private static List<Pessoa> pessoas = new ArrayList<>();

	public static void incluir(Pessoa pessoa) {
		pessoas.add(pessoa);
		FileUtil.toDisk(CSVUtil.toCsv(pessoas));
	}

	public static Pessoa encontrarPorCpf(String cpf) throws NegocioException {
		for (Pessoa pessoa : pessoas) {
			if (cpf.equals(pessoa.getCpf())) {
				return pessoa;
			}
		}
		throw new NegocioException("Nenhuma pessoa encontrada para o CPF especificado.");
	}

}
