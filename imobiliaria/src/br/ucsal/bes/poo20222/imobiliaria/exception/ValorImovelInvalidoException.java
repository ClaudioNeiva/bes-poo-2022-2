package br.ucsal.bes.poo20222.imobiliaria.exception;

public class ValorImovelInvalidoException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public ValorImovelInvalidoException(String message) {
		super(message);
	}
}
