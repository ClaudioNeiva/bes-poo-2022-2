package br.ucsal.bes.poo20222.imobiliaria;

import br.ucsal.bes.poo20222.imobiliaria.domain.Sobrado;
import br.ucsal.bes.poo20222.imobiliaria.exception.ValorImovelInvalidoException;

public class Exemplo {

	public static void main(String[] args) {
		try {
			Sobrado sobrado = new Sobrado(1, "ruaz", "bairro y", 100d, 500d, 200d, 3);
			sobrado.calcularValorImposto();
		} catch (ValorImovelInvalidoException e) {
			System.out.println(e.getMessage());
		}

	}
}
