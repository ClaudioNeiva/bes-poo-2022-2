package br.ucsal.bes.poo20222.imobiliaria.domain;

import br.ucsal.bes.poo20222.imobiliaria.exception.ValorImovelInvalidoException;

public class Terreno extends Imovel {

	private Double area;

	public Terreno(Integer codigo, String endereco, String bairro, Double valor, Double area)
			throws ValorImovelInvalidoException {
		super(codigo, endereco, bairro, valor);
		this.area = area;
	}

	@Override
	public Double calcularValorImposto() {
		return 50 * area;
	}

	public Double getArea() {
		return area;
	}

	public void setArea(Double area) {
		this.area = area;
	}

	@Override
	public String toString() {
		return "Terreno [area=" + area + ", toString()=" + super.toString() + "]";
	}

}
