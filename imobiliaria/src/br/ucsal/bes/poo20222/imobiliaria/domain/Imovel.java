package br.ucsal.bes.poo20222.imobiliaria.domain;

import br.ucsal.bes.poo20222.imobiliaria.exception.ValorImovelInvalidoException;

/*
 (validação) É importante observar que o valor do imóvel deve ser sempre maior que zero.
 */
public abstract class Imovel {

	private Integer codigo;
	private String endereco;
	private String bairro;
	private Double valor;

	public Imovel(Integer codigo, String endereco, String bairro, Double valor) throws ValorImovelInvalidoException {
		setCodigo(codigo);
		setEndereco(endereco);
		setBairro(bairro);
		setValor(valor);
	}
	
	public abstract Double calcularValorImposto();
	
	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) throws ValorImovelInvalidoException {
		validarValor(valor);
		this.valor = valor;
	}

	private void validarValor(Double valor) throws ValorImovelInvalidoException {
		if (valor <= 0) {
			throw new ValorImovelInvalidoException("O valor do imóvel deve ser maior que zero.");
		}
	}

	@Override
	public String toString() {
		return "Imovel [codigo=" + codigo + ", endereco=" + endereco + ", bairro=" + bairro + ", valor=" + valor + "]";
	}

}
