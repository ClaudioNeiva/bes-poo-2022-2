package br.ucsal.bes.poo20222.imobiliaria.domain;

import br.ucsal.bes.poo20222.imobiliaria.exception.ValorImovelInvalidoException;

public class Sobrado extends Casa {

	private Integer qtdAndares;

	public Sobrado(Integer codigo, String endereco, String bairro, Double valor, Double areaTerreno,
			Double areaConstruida, Integer qtdAndares) throws ValorImovelInvalidoException {
		super(codigo, endereco, bairro, valor, areaTerreno, areaConstruida);
		this.qtdAndares = qtdAndares;
	}
	
	@Override
	public Double calcularValorImposto() {
		return getAreaConstruida() * 10 + 50 * qtdAndares;
	}

	public Integer getQtdAndares() {
		return qtdAndares;
	}

	public void setQtdAndares(Integer qtdAndares) {
		this.qtdAndares = qtdAndares;
	}

	@Override
	public String toString() {
		return "Sobrado [qtdAndares=" + qtdAndares + ", toString()=" + super.toString() + "]";
	}

}
