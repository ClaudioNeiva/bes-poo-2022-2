package br.ucsal.bes.poo20222.imobiliaria.domain;

import br.ucsal.bes.poo20222.imobiliaria.exception.ValorImovelInvalidoException;

public class Apartamento extends Imovel {

	private Double areaFracaoIdeal;
	private Double areaPrivativa;

	public Apartamento(Integer codigo, String endereco, String bairro, Double valor, Double areaFracaoIdeal,
			Double areaPrivativa) throws ValorImovelInvalidoException {
		super(codigo, endereco, bairro, valor);
		this.areaFracaoIdeal = areaFracaoIdeal;
		this.areaPrivativa = areaPrivativa;
	}

	@Override
	public Double calcularValorImposto() {
		return 130 * areaPrivativa + 40 * areaFracaoIdeal;
	}

	public Double getAreaFracaoIdeal() {
		return areaFracaoIdeal;
	}

	public void setAreaFracaoIdeal(Double areaFracaoIdeal) {
		this.areaFracaoIdeal = areaFracaoIdeal;
	}

	public Double getAreaPrivativa() {
		return areaPrivativa;
	}

	public void setAreaPrivativa(Double areaPrivativa) {
		this.areaPrivativa = areaPrivativa;
	}

	@Override
	public String toString() {
		return "Apartamento [areaFracaoIdeal=" + areaFracaoIdeal + ", areaPrivativa=" + areaPrivativa + ", toString()="
				+ super.toString() + "]";
	}

}
