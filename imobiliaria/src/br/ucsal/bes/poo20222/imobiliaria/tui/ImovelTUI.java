package br.ucsal.bes.poo20222.imobiliaria.tui;

import java.util.Scanner;

import br.ucsal.bes.poo20222.imobiliaria.domain.Apartamento;
import br.ucsal.bes.poo20222.imobiliaria.domain.Casa;
import br.ucsal.bes.poo20222.imobiliaria.domain.Imovel;
import br.ucsal.bes.poo20222.imobiliaria.exception.ValorImovelInvalidoException;
import br.ucsal.bes.poo20222.imobiliaria.persistence.ImovelDAO;

public class ImovelTUI {

	private static Scanner scanner = new Scanner(System.in);

	public static void cadastrarCasa() {
		System.out.println("*********** CADASTRO DE CASA ***********");

		System.out.println("Informe o código:");
		Integer codigo = scanner.nextInt();

		System.out.println("Informe o endereço:");
		String endereco = scanner.nextLine();

		System.out.println("Informe o bairro:");
		String bairro = scanner.nextLine();

		System.out.println("Informe o valor:");
		Double valor = scanner.nextDouble();

		System.out.println("Informe a área do terreno:");
		Double areaTerreno = scanner.nextDouble();

		System.out.println("Informe a área construída:");
		Double areaConstruida = scanner.nextDouble();

		try {
			Casa casa = new Casa(codigo, endereco, bairro, valor, areaTerreno, areaConstruida);
			ImovelDAO.incluir(casa);
		} catch (ValorImovelInvalidoException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void cadastrarApartamento() {
		System.out.println("*********** CADASTRO DE APARTAMENTO ***********");

		System.out.println("Informe o código:");
		Integer codigo = scanner.nextInt();

		System.out.println("Informe o endereço:");
		String endereco = scanner.nextLine();

		System.out.println("Informe o bairro:");
		String bairro = scanner.nextLine();

		System.out.println("Informe o valor:");
		Double valor = scanner.nextDouble();

		System.out.println("Informe a área de fração ideal:");
		Double areaFracaoIdeal = scanner.nextDouble();

		System.out.println("Informe a área privativa:");
		Double areaPrivativa = scanner.nextDouble();

		try {
			Apartamento apartamento = new Apartamento(codigo, endereco, bairro, valor, areaFracaoIdeal, areaPrivativa);
			ImovelDAO.incluir(apartamento);
		} catch (ValorImovelInvalidoException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void exibirImoveisPorBairroValor() {
		System.out.println("*********** LISTAGEM DE IMÓVEIS POR BAIRRO E VALOR ***********");
		for (Imovel imovel : ImovelDAO.obterTodos()) {
			System.out.println("\n\n");
			System.out.println("Código: " + imovel.getCodigo());
			System.out.println("Endereço: " + imovel.getEndereco());
			System.out.println("Valor: " + imovel.getValor());
			System.out.println("Valor do imposto: " + imovel.calcularValorImposto());
//			if (imovel instanceof Casa casa) {
//				System.out.println("Valor do imposto: " + casa.calcularValorImposto());
//			} else if (imovel instanceof Apartamento apartamento) {
//				System.out.println("Valor do imposto: " + apartamento.calcularValorImposto());
//			}
		}
	}

}
