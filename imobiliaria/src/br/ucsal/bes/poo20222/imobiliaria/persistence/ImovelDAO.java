package br.ucsal.bes.poo20222.imobiliaria.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes.poo20222.imobiliaria.domain.Imovel;

public class ImovelDAO {

	private static List<Imovel> imoveis = new ArrayList<>();

	public static void incluir(Imovel imovel) {
		imoveis.add(imovel);
	}

	public static List<Imovel> obterTodos() {
		return new ArrayList<>(imoveis);
	}

}
