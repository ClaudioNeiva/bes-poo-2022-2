package br.ucsal.bes.poo20222.aula14;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Exemplo1 {

	public static void main(String[] args) {
		
		List<String> nomes = Arrays.asList("claudio", "antonio", "Neiva", "pedreira", "joaquim");

		System.out.println("\nExibir os nomes na ordem que foram informados:");
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println("\nExibir os nomes na ordem crescente:");
		nomes.sort(Comparator.naturalOrder());
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println("\nExibir os nomes na ordem crescente:");
		nomes.sort(Comparator.naturalOrder());
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println("\nExibir os nomes na ordem decrescente:");
		nomes.sort(Comparator.reverseOrder());
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println("\nExibir os nomes na ordem crescente \"case insensitive\":");
		nomes.sort(String.CASE_INSENSITIVE_ORDER);
		for (String nome : nomes) {
			System.out.println(nome);
		}

		System.out.println("\nExibir os nomes na ordem decrescente \"case insensitive\":");
		nomes.sort(String.CASE_INSENSITIVE_ORDER.reversed());
		for (String nome : nomes) {
			System.out.println(nome);
		}

//		System.out.println("Exibir os nomes na ordem que foram informados:");
//		for (int i = 0; i < nomes.size(); i++) {
//			String nome = nomes.get(i);
//			System.out.println(nome);
//		}
//
//		System.out.println("Exibir os nomes na ordem que foram informados:");
//		for (String nome : nomes) {
//			System.out.println(nome);
//		}
//
//		System.out.println("Exibir os nomes na ordem que foram informados:");
//		nomes.forEach(nome -> System.out.println(nome));
//		
//		System.out.println("Exibir os nomes na ordem que foram informados:");
//		nomes.forEach(System.out::println);

//		List<String> nomes2 = new ArrayList<>();
//		nomes2.add("claudio");
//		nomes2.add("antonio");
//		nomes2.add("neiva");
//		nomes2.add("pedreira");
//		nomes2.add("joaquim");

		// @formatter:off
		
		/* 
		System.out.println("Exibir os nomes na ordem que foram informados:");
		for (String nome : nomes) {
			System.out.println(nome);
		}
		 */
		
		// @formatter:on

	}

}
