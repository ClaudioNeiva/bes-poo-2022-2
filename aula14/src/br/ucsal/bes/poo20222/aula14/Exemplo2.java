package br.ucsal.bes.poo20222.aula14;

import java.util.ArrayList;
//import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Exemplo2 {

	public static void main(String[] args) {
		List<Aluno> alunos = new ArrayList<>();

		alunos.add(new Aluno(1, 2000, "clara"));
		alunos.add(new Aluno(5, 2001, "ana"));
		alunos.add(new Aluno(4, 2000, "maria"));
		alunos.add(new Aluno(9, 1990, "pedro"));
		alunos.add(new Aluno(6, 2000, "joaquim"));
		alunos.add(new Aluno(8, 2001, "claudio"));

		System.out.println("\nAlunos na ordem que foram adicionados à lista:");
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

		System.out.println("\nAlunos na ordem crescente nome:");
		alunos.sort(Comparator.comparing(Aluno::getNome));
//		Collections.sort(alunos, new Comparator<>() {
//			@Override
//			public int compare(Aluno o1, Aluno o2) {
//				return o1.getNome().compareTo(o2.getNome());
//			}
//		});
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

		System.out.println("\nAlunos na ordem crescente matrícula:");
		alunos.sort(Comparator.comparing(Aluno::getMatricula));
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

		System.out.println("\nAlunos na ordem crescente anos de nascimento e nome:");
		alunos.sort(Comparator.comparing(Aluno::getAnoNascimento).thenComparing(Aluno::getNome));
//		Collections.sort(alunos, new Comparator<>() {
//			@Override
//			public int compare(Aluno o1, Aluno o2) {
//				int result = o1.getAnoNascimento().compareTo(o2.getAnoNascimento());
//				if(result == 0) {
//					result = o1.getNome().compareTo(o2.getNome());
//				}
//				return result;
//			}
//		});
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

		System.out.println("\nAlunos na ordem crescente PADRÃO para a classe Aluno:");
		alunos.sort(Comparator.naturalOrder());
		for (Aluno aluno : alunos) {
			System.out.println(aluno);
		}

	}

}
