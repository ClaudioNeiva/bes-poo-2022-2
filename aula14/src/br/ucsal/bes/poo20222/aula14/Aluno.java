package br.ucsal.bes.poo20222.aula14;

public class Aluno implements Comparable<Aluno>{

	private Integer matricula;

	private Integer anoNascimento;

	private String nome;

	public Aluno(Integer matricula, Integer anoNascimento, String nome) {
		this.matricula = matricula;
		this.anoNascimento = anoNascimento;
		this.nome = nome;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Aluno [matricula=" + matricula + ", anoNascimento=" + anoNascimento + ", nome=" + nome + "]";
	}

	// "Ordem natural" para comparação de alunos é a "por matrícula".
	@Override
	public int compareTo(Aluno o) {
		return matricula.compareTo(o.matricula);
	}

}
