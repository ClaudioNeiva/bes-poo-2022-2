package br.ucsal.bes.poo20222.aula14;

public interface MinhaInterface {

	// public abstract void fazerAlgo();
	default void fazerAlgo() {
		System.out.println("colocar um comportamento em um método de uma interface");
	}
	
}
