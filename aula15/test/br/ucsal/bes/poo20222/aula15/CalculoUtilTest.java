package br.ucsal.bes.poo20222.aula15;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class CalculoUtilTest {

	@ParameterizedTest
	@CsvSource({ "0,1", "1,1", "3,6", "5,120" })
	void testarFatorial(int n, long fatEsperado) {
		long fatAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatEsperado, fatAtual);
	}

//	@Test
//	void testarFatorial0() {
//		int n = 0;
//		long fatEsperado = 1;
//		long fatAtual = CalculoUtil.calcularFatorial(n);
//		Assertions.assertEquals(fatEsperado, fatAtual);
//	}
//
//	@Test
//	void testarFatorial1() {
//		int n = 1;
//		long fatEsperado = 1;
//		long fatAtual = CalculoUtil.calcularFatorial(n);
//		Assertions.assertEquals(fatEsperado, fatAtual);
//	}
//
//	@Test
//	void testarFatorial3() {
//		int n = 3;
//		long fatEsperado = 6;
//		long fatAtual = CalculoUtil.calcularFatorial(n);
//		Assertions.assertEquals(fatEsperado, fatAtual);
//	}
//
//	@Test
//	void testarFatorial5() {
//		int n = 5;
//		long fatEsperado = 120;
//		long fatAtual = CalculoUtil.calcularFatorial(n);
//		Assertions.assertEquals(fatEsperado, fatAtual);
//	}

}
