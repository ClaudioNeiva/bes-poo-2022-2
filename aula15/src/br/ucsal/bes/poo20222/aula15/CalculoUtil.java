package br.ucsal.bes.poo20222.aula15;

public class CalculoUtil {

	public static long calcularFatorial(int n) {
		if (n == 0) {
			return 1;
		}
		return n * calcularFatorial(n - 1);
	}

}
