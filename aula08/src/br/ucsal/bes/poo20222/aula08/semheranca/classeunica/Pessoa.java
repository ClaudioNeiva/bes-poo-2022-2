package br.ucsal.bes.poo20222.aula08.semheranca.classeunica;

import java.util.ArrayList;
import java.util.List;

//NÃO FAÇA ASSIM!!!!
public class Pessoa {

	private String cnpj;
	private String inscricaoEstadual;
	private String inscricaoMunicipal;
	private String nome;
	private String endereco;
	private List<String> telefones;
	private String cpf;
	private String nomeMae;

	public Pessoa(String cnpj, String inscricaoEstadual, String inscricaoMunicipal, String nome, String endereco,
			List<String> telefones, String cpf, String nomeMae) {
		super();
		this.cnpj = cnpj;
		this.inscricaoEstadual = inscricaoEstadual;
		this.inscricaoMunicipal = inscricaoMunicipal;
		this.nome = nome;
		this.endereco = endereco;
		this.telefones = telefones;
		this.cpf = cpf;
		this.nomeMae = nomeMae;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getInscricaoMunicipal() {
		return inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public List<String> getTelefones() {
		return new ArrayList<>(telefones);
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

}
