package br.ucsal.bes.poo20222.aula12;

import java.time.LocalDate;

public class ContaCorrente {

	protected static final String MENS_SALDO_INSUFICIENTE = "Saldo insuficiente. Não é possível executar o saque.";

	private static int cont = 0;

	private int codigo;

	private LocalDate dataAbertura;

	private String nomeCorrentista;

	protected double saldo;

	public ContaCorrente(String nomeCorrentista) {
		this.nomeCorrentista = nomeCorrentista;
		definirCodigo();
		definirDataAbertura();
		definirSaldoInicial();
	}
	
	public static String retornarMensagem() {
		return "sou comum";
	}

	public void depositar(double valor) {
		saldo += valor;
	}

	public void sacar(double valor) throws NegocioException {
		if(valor > saldo) {
			throw new NegocioException(MENS_SALDO_INSUFICIENTE);
		}
		saldo -= valor;
	}

	public String getNomeCorrentista() {
		return nomeCorrentista;
	}

	public void setNomeCorrentista(String nomeCorrentista) {
		this.nomeCorrentista = nomeCorrentista;
	}

	public int getCodigo() {
		return codigo;
	}

	public LocalDate getDataAbertura() {
		return dataAbertura;
	}

	public double getSaldo() {
		return saldo;
	}

	private void definirSaldoInicial() {
		saldo = 0;
	}

	private void definirDataAbertura() {
		dataAbertura = LocalDate.now();
	}

	private void definirCodigo() {
		ContaCorrente.cont++;
		codigo = cont;
	}

}
