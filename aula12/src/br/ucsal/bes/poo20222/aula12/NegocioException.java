package br.ucsal.bes.poo20222.aula12;

public class NegocioException extends Exception {

	private static final long serialVersionUID = 1L;

	public NegocioException(String message) {
		super(message);
	}

}
