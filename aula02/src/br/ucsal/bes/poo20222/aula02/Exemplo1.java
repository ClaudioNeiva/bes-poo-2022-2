package br.ucsal.bes.poo20222.aula02;

import java.time.LocalDate;

public class Exemplo1 {

	static int numero;
	static int[] numeros = new int[10]; // tipo de dado composto homogêneo
	static Aluno aluno1 = new Aluno();
	static Aluno[] aluno2 = new Aluno[100];

	public static void main(String[] args) {
		numero = 50;

		numeros[0] = 5;
		numeros[3] = 8;
		numeros[7] = 1235;

		aluno1.matricula = 123;
		aluno1.nome = "claudio neiva";
		aluno1.email = "antonioclaudioneiva@gmail.com";
		aluno1.dataNascimento = LocalDate.of(2000, 1, 3);

		Turma turma1 = new Turma();
		// turma1.alunos[2] = aluno1;

		Aluno aluno2 = new Aluno();
		Aluno aluno3 = new Aluno();
		Aluno aluno4 = new Aluno();

		aluno2.nome = "maria";
		aluno3.nome = "joão";
		aluno4.nome = "pedro";

		System.out.println("aluno2.nome=" + aluno2.nome);
		System.out.println("aluno3.nome=" + aluno3.nome);
		System.out.println("aluno4.nome=" + aluno4.nome);

	}

}
