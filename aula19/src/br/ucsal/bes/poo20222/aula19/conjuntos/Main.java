package br.ucsal.bes.poo20222.aula19.conjuntos;

import java.time.LocalDate;

public class Main {

	public static void main(String[] args) {
		Aluno aluno1 = new Aluno(1, "claudio", LocalDate.of(2000, 1, 3));
		System.out.println(aluno1.hashCode());
	}
	
}
