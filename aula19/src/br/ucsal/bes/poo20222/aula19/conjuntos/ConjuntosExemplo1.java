package br.ucsal.bes.poo20222.aula19.conjuntos;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class ConjuntosExemplo1 {

	private static final int QTD_NOMES = 5;
	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {

		// Melhor performance para inclusão e recuperação de elementos no conjunto.
		Set<String> nomesDistintosConjunto = new HashSet<>();

		// Garante que a ordem dos elementos, numa varredura do conjunto, é a mesma
		// ordem na qual os elementos foram inseridos no conjunto.
		// Set<String> nomesDistintosConjunto = new LinkedHashSet<>();
		
		// Ordena os elementos enquanto os coloca no conjunto.
		// Set<String> nomesDistintosConjunto = new TreeSet<>();
		
		String nome;

		System.out.println("Informe " + QTD_NOMES + " nomes distintos:");
		do {
			nome = scanner.nextLine();
			nomesDistintosConjunto.add(nome);
		} while (nomesDistintosConjunto.size() < QTD_NOMES);

		for (String nome2 : nomesDistintosConjunto) {
		}

		System.out.println("Nomes distintos informados:");
		nomesDistintosConjunto.forEach(System.out::println);

	}
}
