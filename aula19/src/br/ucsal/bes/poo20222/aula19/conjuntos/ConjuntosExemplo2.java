package br.ucsal.bes.poo20222.aula19.conjuntos;

import java.time.LocalDate;
import java.util.Set;
import java.util.TreeSet;

public class ConjuntosExemplo2 {

	public static void main(String[] args) {

		Set<Aluno> alunos = new TreeSet<>();

		Aluno aluno1 = new Aluno(1, "claudio", LocalDate.of(2000, 2, 1));
		Aluno aluno2 = new Aluno(5, "ana", LocalDate.of(2001, 4, 12));
		Aluno aluno3 = new Aluno(3, "clara", LocalDate.of(2001, 4, 11));
		Aluno aluno4 = new Aluno(2, "pedro", LocalDate.of(2004, 7, 2));
		Aluno aluno5 = new Aluno(5, "ana", LocalDate.of(2001, 4, 12));

		System.out.println("aluno1.hashCode=" + aluno1.hashCode());
		System.out.println("aluno2.hashCode=" + aluno2.hashCode());
		System.out.println("aluno3.hashCode=" + aluno3.hashCode());
		System.out.println("aluno4.hashCode=" + aluno4.hashCode());
		System.out.println("aluno5.hashCode=" + aluno5.hashCode());
		System.out.println();

		alunos.add(aluno1);
		alunos.add(aluno2);
		alunos.add(aluno3); // armazenou o valor atual do hashCode e associou com o aluno3
		alunos.add(aluno4);
		alunos.add(aluno5);

		alunos.forEach(System.out::println);

		System.out.println("\nalunos.contains(aluno3)=" + alunos.contains(aluno3));

		aluno3.setNome("leila");
		System.out.println("aluno3=" + aluno3);
		System.out.println("aluno3.hashCode=" + aluno3.hashCode() + "\n");

		alunos.forEach(System.out::println);

		// Procurou pelo novo hashCode de aluno3 e não achou, pq no hashtable estava o
		// hashCode antigo
		System.out.println("\nalunos.contains(aluno3)=" + alunos.contains(aluno3));
		System.out.println("alunos.remove(aluno3)=" + alunos.remove(aluno3) + "\n");

		alunos.forEach(System.out::println);

	}

}
