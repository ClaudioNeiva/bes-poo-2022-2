package br.ucsal.bes.poo20222.aula19.mapas;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class MapasExemplo1 {

	private static final int QTD = 5;
	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		// Solicite do usuário 5 números e retorne os distintos números e quantas vezes
		// eles foram informados.
		// Exemplo: ENTRADA => 8, 7, 8, 8, 7 ; SAIDA => 8 x 3, 7 x 2

		// distintosQtdSEMMapas();
		distintosQtdComMapas();
	}

	private static void distintosQtdComMapas() {
		// Map<distintos, qtds> | Map<key, value>
		// Map<Integer, Integer> distintosQtdsMap = new HashMap<>();
		Map<Integer, Integer> distintosQtdsMap = new TreeMap<>(); // Garantir a listagem das chaves em ordem crescente

		Integer n;
		System.out.println("Informe " + QTD + " números:");
		for (int i = 0; i < QTD; i++) {
			n = scanner.nextInt();
			distintosQtdsMap.putIfAbsent(n, 0);
			distintosQtdsMap.put(n, distintosQtdsMap.get(n) + 1);
		}

		System.out.println("Números distintos x qtd ocorrências:");
		for (Integer key : distintosQtdsMap.keySet()) {
			System.out.println(key + " x " + distintosQtdsMap.get(key));
		}
	}

	private static void distintosQtdSEMMapas() {
		int index;
		Integer n;
		List<Integer> distintos = new ArrayList<>();
		List<Integer> qtds = new ArrayList<>();

		System.out.println("Informe " + QTD + " números:");
		for (int i = 0; i < QTD; i++) {
			n = scanner.nextInt();
			index = distintos.indexOf(n);
			if (index >= 0) { // index >= 0 significa que o elemento existe na lista, na posição de index
				Integer qtdAtual = qtds.get(index);
				Integer qtdNova = qtdAtual + 1;
				qtds.set(index, qtdNova);
			} else { // index < 0 significa que o elemento NÃO existe na lista
				distintos.add(n);
				qtds.add(1);
			}
		}

		System.out.println("Números distintos x qtd ocorrências:");
		for (int i = 0; i < distintos.size(); i++) {
			System.out.println(distintos.get(i) + " x " + qtds.get(i));
		}
	}

}
